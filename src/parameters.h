#pragma once

#include "ofRectangle.h"
#include "ofXml.h"
#include "ofxXmlSettings.h"

class PARAMETERS
{
public:
	PARAMETERS();
	~PARAMETERS();

	bool readCamMap();
	bool readConfig(const string & fileName);
	void setDefaultConfig();
	void saveToXml(const vector<ofRectangle> &frames, const string &name="");
	bool loadFromXml(vector<ofRectangle> &frames);

	vector<vector<int>> camMap;
	string xmlFileName;
	bool useXml;
	bool useMap;
	string mapFile;
	string warpFileName;
	bool useWarpFile;
	float warpEdge; // 0..1
	int NCHANNEL;
	string oscXml;
	string ipCamXml;

	ofXml xml;
	ofxXmlSettings xmlLoad;
};

