#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main() {

	//ofSetupOpenGL(1800, 1000, OF_WINDOW);			// <-------- setup the GL context


	ofGLFWWindowSettings settings;
	settings.setGLVersion(3, 3);
	settings.width = 1600;// ofGetWidth();
	settings.height = 900;// ofGetHeight();
	ofCreateWindow(settings);


	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new ofApp());

}
