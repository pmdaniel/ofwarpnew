#include "blobtracker.h"

BLOBTRACKER::BLOBTRACKER(ofImage *img){
	reset(img);
}

BLOBTRACKER::BLOBTRACKER(ofImage *img, const BLOBPARAMS &bp, const string &oscXml) {
	oscXmlName = oscXml;
	reset(img);
	threshold = bp.thres;
	blur = bp.blur;
	blobsManager.normalizePercentage = bp.normperc;
	blobsManager.maxUndetectedTime = bp.maxUndetectedTime;
	blobsManager.minDetectedTime = bp.minDetectedTime;
}

BLOBTRACKER::~BLOBTRACKER(){
}

void BLOBTRACKER::reset(ofImage *img) {
	srcImg = img;
	init = true;
	reset();
}

void BLOBTRACKER::reset() {
	init = false;
	bLearnBakground = true;

	width = srcImg->getWidth();
	height = srcImg->getHeight();

	pixels.allocate(width, height, ofImageType::OF_IMAGE_GRAYSCALE);
	fboDiff.allocate(width, height, GL_RED);
	backgroundEl.allocate(width, height, ofImageType::OF_IMAGE_GRAYSCALE);

	colorImg.allocate(width, height);
	grayImage.allocate(width, height);
	grayBg.allocate(width, height);
	grayDiff.allocate(width, height);

	//bLearnBakground = false;

	//threshold = 1;
	//blur = 10;

//	blobsManager.normalizePercentage = 0.3;
	blobsManager.giveLowestPossibleIDs = true;
//	blobsManager.maxUndetectedTime = 0;// 1500;
//	blobsManager.minDetectedTime = 0;// 1500;
	blobsManager.debugDrawCandidates = true;

	initGui();

	// OSC
	loadOscData();
	oscOn = true;
	blobSender.clear();
	for (int i = 0; i < oscData.size(); i++) {
		ofxOscSender oos;
		oos.setup(oscData[i].ip, oscData[i].port);
		blobSender.push_back(oos);
	}
}

void BLOBTRACKER::update(){
	if (init)
		reset();
	background.setDifferenceMode(ofxCv::RunningBackground::DifferenceMode(0));
	background.setLearningTime(60);
	background.setThresholdValue(100);
	background.update(*srcImg, backgroundEl);
	backgroundEl.update();

	fboDiff.begin();
	backgroundEl.draw(0, 0);
	fboDiff.end();

	fboDiff.readToPixels(pixels);

	grayImage.setFromPixels(pixels);
	if (bLearnBakground == true) {
		grayBg = grayImage;
	}

	grayDiff = grayImage;
	grayDiff.blur(blur);
	grayDiff.threshold(threshold);

	contourFinder.findContours(grayDiff, 50, (width * height) / 3, 10, false);

	blobsManager.update(contourFinder.blobs);
	
	// to be executed on separate thread
	if(oscOn)
		sendBlobs();
}

void BLOBTRACKER::initGui() {
	guiLbl.setup();
	guiDec.setup();
	guiInc.setup();

	guiNames.resize(0);
	guiNames.push_back("threshold");
	guiNames.push_back("blur");
	guiNames.push_back("normalizePercentage");
	guiNames.push_back("maxUndetectedTime");
	guiNames.push_back("minDetectedTime");

	int nparam = guiNames.size();

	labels.resize(nparam);
	btnDec.resize(nparam);
	btnInc.resize(nparam);

	for (int i = 0; i < nparam; i++) {
		guiLbl.add(labels[i].setup(guiNames[i]));
		guiDec.add(btnDec[i].setup("-"));
		guiInc.add(btnInc[i].setup("+"));
	}
	setGuiValues();
}

void BLOBTRACKER::drawGui(const ofVec2f &place) {
	guiLbl.setPosition(place);
	guiInc.setPosition(guiDec.getPosition() + ofPoint(guiDec.getWidth(), 0));
	guiDec.setPosition(guiLbl.getPosition() + ofPoint(guiLbl.getWidth(), 0));

	guiLbl.draw();
	guiDec.draw();
	guiInc.draw();
}

void BLOBTRACKER::guiBtnAction() {
	int i = 0;
	int change = 0;
	for (; i < labels.size(); i++) {
		ofMouseEventArgs ea;
		ea.button = 0;
		if (btnDec[i].mouseReleased(ea)) {
			change = -1;
			break;
		}
		else if (btnInc[i].mouseReleased(ea)) {
			change = 1;
			break;
		}
	}

	switch (i) {
	case 0:
		threshold += change;
		break;
	case 1:
		blur += change;
		if (blur < 0)
			blur = 0;
		break;
	case 2:
		blobsManager.normalizePercentage += (float)change/10;
		break;
	case 3:
		blobsManager.maxUndetectedTime += change;
		break;
	case 4:
		blobsManager.minDetectedTime += change;
		break;
	}

	setGuiValues();
}

void BLOBTRACKER::setGuiValues() {
	labels[0].setup(guiNames[0], ofToString(threshold));
	labels[1].setup(guiNames[1], ofToString(blur));
	labels[2].setup(guiNames[2], ofToString(blobsManager.normalizePercentage));
	labels[3].setup(guiNames[3], ofToString(blobsManager.maxUndetectedTime));
	labels[4].setup(guiNames[4], ofToString(blobsManager.minDetectedTime));
}

void BLOBTRACKER::saveParams(BLOBPARAMS &bp) {
	if(this)
		bp.set(threshold, blur, blobsManager.normalizePercentage, blobsManager.maxUndetectedTime, blobsManager.minDetectedTime);
}

void BLOBTRACKER::sendBlobs() {
	ofxOscMessage m;
	for (int i = 0; i < blobsManager.blobs.size(); i++) {
		m.clear();
		m.setAddress("/blob"); m.addInt32Arg(blobsManager.blobs[i].id);
		m.addFloatArg(blobsManager.blobs[i].centroid.x / width);
		m.addFloatArg(blobsManager.blobs[i].centroid.y / height);
		for (int j = 0; j < blobSender.size(); j++) {
			blobSender[j].sendMessage(m, false);
		}
	}
}


///////////--------------- BLOBPARAMS

void BLOBPARAMS::set(const BLOBPARAMS &bp) {
	thres = bp.thres;
	blur = bp.blur;
	normperc = bp.normperc;
	maxUndetectedTime = bp.maxUndetectedTime;
	minDetectedTime = bp.minDetectedTime;
}

void BLOBPARAMS::set(int th, int bl, float np, int mut, int mdt) {
	thres = th;
	blur = bl;
	normperc = np;
	maxUndetectedTime = mut;
	minDetectedTime = mdt;
}

void BLOBPARAMS::setDefaults() {
	thres = 1;
	blur = 10;
	normperc = 0.3;
	maxUndetectedTime = 0;
	minDetectedTime = 0;

}

bool BLOBTRACKER::loadOscData() {
	ofxXmlSettings xmlSettings;
	if (!xmlSettings.load(oscXmlName)) {
		cout << "Error loading XML." << endl;
		return false;
	}
	oscData.clear();
	string str = "oscdata";
	if (xmlSettings.getNumTags(str) > 0) {
		xmlSettings.pushTag(str);
		str = "host";
		int nhost = xmlSettings.getNumTags(str);
		for (int i = 0; i < nhost; i++) {
			cout << i << endl;
			oscData.push_back(OSCDATA(
				xmlSettings.getValue(str + ":ip", "", i),
				xmlSettings.getValue(str + ":name", "", i),
				xmlSettings.getValue(str + ":port", 0, i)
			));
		}
	}

	return true;
}