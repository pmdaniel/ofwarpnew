#pragma once
#include "pmswitches.h"

#include "ofMain.h"
#include "ofxGui.h"
#if USEWARP
#include "ofxWarp.h"
#endif // USEWARP
#include "parameters.h"
#include "blobtracker.h"
#if IPCAM
#include "IPVideoGrabber.h"
#endif //IPCAM


// aux functions
ofRectangle getBoundaryRct(const vector<ofRectangle> &rcts);
ofRectangle getBoundaryRct(const ofRectangle & rct, const ofPoint& pnt);
#if USEWARP
ofRectangle getWarpBoundary(shared_ptr<ofxWarpBase> warp, bool norm);
#endif

class ofApp : public ofBaseApp {
public:
	void setup();
	void update();
	void draw();
	//void exit();
	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
#if USEWARP
	string createJson(int n, bool framePos);
	bool generateWarp(const int& nWarp);
#endif
	void initResizeMembers();
	void initMapLayout();
	void generateMerged();
	inline bool isValidIdx(const int& idx);
	~ofApp();

	ofTrueTypeFont font;
	PARAMETERS params;
	bool changedPositions;

#if VID_INSTEAD_CAM
	ofVideoPlayer *vidGrabber;
#else
#if IPCAM
	ofx::Video::IPVideoGrabber *vidGrabber;
	bool LoadIpCams(const string &xmlName, vector<float> &ws, vector<float> &hs);
#else
	ofVideoGrabber *vidGrabber;
#endif // IPCAM
#endif //VID_INSTEAD_CAM

	// options
	bool isPaused;
	int currentFrame; // the one which is under the mouse cursor
	bool warpOn;
	bool showFrames;
	bool resetWarp;
	bool highlightCurrent;
	bool grayResult;
	bool dragAll;
	void initOptions();

	ofImage *imgs; // params.NCHANNEL
	vector<ofRectangle> frames;
	ofImage imgMerged;

	int nsaved;

	//// dragging
	ofPoint dragStart;
	ofPoint dragRel;
	ofPoint dragPnt;
	bool drawDrag;
	int whichToDrag;

	// GUI parameters
	ofxPanel guiLbl;
	ofxPanel guiDec;
	ofxPanel guiInc;
	ofxLabel *xlbls; // params.NCHANNEL
	ofxLabel *ylbls; // params.NCHANNEL
	ofxButton *btnDec; // 2 * params.NCHANNEL
	ofxButton *btnInc; // 2 * params.NCHANNEL
	void initGui();
	void relocateGui();
	void guiBtnAction();
	void guiUpdateLabels();

#if USEWARP
	// stuff for warping
	ofxWarpController warpController;
	vector<shared_ptr<ofxWarpBase>> warp;
	ofTexture *textures;
	ofFbo *warpFbo; // params.NCHANNEL
	ofRectangle *warpSrc;
	ofRectangle *warpDest;
	ofPoint *offsetWarp;
	bool doCrop;
#endif

					// blob tracking
	bool showBlobs;
	BLOBTRACKER *bt;
	BLOBPARAMS bp;


}; 