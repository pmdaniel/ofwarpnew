#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::initResizeMembers() {
	if (params.NCHANNEL < 0) return;
#if VID_INSTEAD_CAM
	vidGrabber = new ofVideoPlayer[params.NCHANNEL];
#else
#if !IPCAM
	vidGrabber = new ofVideoGrabber[params.NCHANNEL];
#endif // !IPCAM
#endif // VID_INSTEAD_CAM
	imgs = new ofImage[params.NCHANNEL];
	if (!params.useXml)
		frames.resize(params.NCHANNEL);

#if USEWARP
	textures = new ofTexture[params.NCHANNEL];
	warpFbo = new ofFbo[params.NCHANNEL];
	warpSrc = new ofRectangle[params.NCHANNEL];
	warpDest = new ofRectangle[params.NCHANNEL];
	offsetWarp = new ofPoint[params.NCHANNEL];
#endif // USEWARP
	xlbls = new ofxLabel[params.NCHANNEL];
	ylbls = new ofxLabel[params.NCHANNEL];
	btnDec = new ofxButton[params.NCHANNEL * 2];
	btnInc = new ofxButton[params.NCHANNEL * 2];
}

//--------------------------------------------------------------
void ofApp::initGui() {
	// temporarily just some semi-random location
	float x = 800, y = 600;

	if (params.NCHANNEL < 0) return;
	guiLbl.setup();// guiLbl.setPosition(x, y); //guiLbl.setDefaultWidth(100);
	guiDec.setup();//	guiDec.setPosition(guiLbl.getPosition() + ofPoint(guiLbl.getWidth(), 0)); //guiDec.setDefaultWidth(32);
	guiInc.setup();// guiInc.setPosition(guiDec.getPosition() + ofPoint(guiDec.getWidth(), 0)); //guiInc.setDefaultWidth(32);
	relocateGui();
	for (int i = 0; i < params.NCHANNEL; i++) {
#if IPCAM
		guiLbl.add(xlbls[i].setup(vidGrabber[i].getCameraName() + " X: ", ofToString(frames[i].x)));
		guiLbl.add(ylbls[i].setup(vidGrabber[i].getCameraName() + " Y: ", ofToString(frames[i].y)));
#else
		guiLbl.add(xlbls[i].setup("Cam " + ofToString(i + 1) + " X: ", ofToString(frames[i].x)));
		guiLbl.add(ylbls[i].setup("Cam " + ofToString(i + 1) + " Y: ", ofToString(frames[i].y)));
#endif // IPCAM
		guiDec.add(btnDec[i].setup("-"));
		guiDec.add(btnDec[i + params.NCHANNEL].setup("-"));
		guiInc.add(btnInc[i].setup("+"));
		guiInc.add(btnInc[i + params.NCHANNEL].setup("+"));
	}
}

//--------------------------------------------------------------
void ofApp::relocateGui() {
	ofRectangle rctAll = getBoundaryRct(frames);
	float x = rctAll.x;
	float y = rctAll.y + rctAll.height;
	guiLbl.setPosition(x, y);
	guiInc.setPosition(guiDec.getPosition() + ofPoint(guiDec.getWidth(), 0));
	guiDec.setPosition(guiLbl.getPosition() + ofPoint(guiLbl.getWidth(), 0));
}

//--------------------------------------------------------------
void ofApp::initOptions() {
	isPaused = false;
	warpOn = false;
	showFrames = false;
	changedPositions = true; // not really an option, but still...
	highlightCurrent = false;
	dragAll = false;
	grayResult = true;
	nsaved = 0;
	showBlobs = true;
#if USEWARP
	doCrop = false;
#endif // USEWARP
}

//--------------------------------------------------------------
void ofApp::setup() {
	if (!params.readConfig("data/pmconfig")) {
		cout << "Could not parse \"pmconfig\". Exiting." << endl;
		ofExit(false);
		return;
	}
	font.load("mono.ttf", 9);
	if (params.useXml) {
		bool xmlLoaded = params.loadFromXml(frames);
		if (!xmlLoaded && params.useMap) {
			cout << "Failed to load " + params.warpFileName + ". Now trying to load camera layout from \"" + params.mapFile + "\"." << endl;
		}
		params.useXml = xmlLoaded;
	}

	// this part needs to be reorganized, when inputs will become cameras
	if (params.useMap) {
		params.useMap = params.readCamMap();
		if (!params.useMap) {
			cout << "Could not parse " + params.mapFile + ". The cameras will appear in a row." << endl;
		}
	}
	initResizeMembers();
	initOptions();

#if VID_INSTEAD_CAM
	//string srcvid = "movies/scene.avi";
	string srcvid = "movies/fingers.mov";
	//string srcvid = "movies/ISTrailer.mp4";
	//string srcvid = "movies/13_semmi.mp4";

	for (int i = 0; i < params.NCHANNEL; i++) {
		if (!vidGrabber[i].load(srcvid)) {
			cout << "Failed to load the video." << endl;
			ofExit(false);
			return;
		}
	}
#else
#if IPCAM
	vector<float> ws;
	vector<float> hs;
	if (!LoadIpCams(params.ipCamXml, ws, hs)) {
		cout << "Error in parsing " + params.ipCamXml + ". Exiting." << endl;
		ofExit(false);
		return;
	}

#else
	
	//grabber.setDeviceID(0);
	for (int i = 0; i < params.NCHANNEL; i++) {
		ofVideoGrabber grabber;
		vector<ofVideoDevice> devices = grabber.listDevices();
		int ndev = devices.size();
		for (int j = 0; j < ndev; j++) {
			if (devices[j].bAvailable) {
				ofLogNotice() << devices[j].id << ": " << devices[j].deviceName;
			}
			else {
				ofLogNotice() << devices[j].id << ": " << devices[j].deviceName << " - unavailable ";
			}
		}
		int idx = i%ndev;

		vidGrabber[i] = grabber;
		vidGrabber[i].setDeviceID(idx);
		int nd = vidGrabber[i].listDevices().size();
		vidGrabber[i].setDesiredFrameRate(60);
		vidGrabber[i].initGrabber(320, 240);
	}
#endif // IPCAM
#endif // VID_INSTEAD_CAM

	ofDisableArbTex();
	ofSetVerticalSync(true);

	for (int i = 0; i < params.NCHANNEL; i++) {
#if VID_INSTEAD_CAM
		vidGrabber[i].setLoopState(OF_LOOP_NORMAL);
		vidGrabber[i].play();
#endif
#if IPCAM
		float w = ws[i];
		float h = hs[i];
#else
		float w = vidGrabber[i].getWidth();
		float h = vidGrabber[i].getHeight();
#endif

		if (!params.useXml) {
			frames[i] = ofRectangle(10, i * (h + 10) + 20, w, h);
		}
		
#if USEWARP
		textures[i].allocate(w, h, GL_RGBA);
		warpFbo[i].allocate(w, h, GL_RGBA);
#else
		imgs[i].allocate(w, h, ofImageType::OF_IMAGE_COLOR_ALPHA);
#endif
	}
	if (!params.useXml && params.useMap) {
		initMapLayout();
	}

	if (!params.useXml) {
		ofPoint moveFrames(30, 30);
		for (int i = 0; i < params.NCHANNEL; i++) {
			frames[i].position += moveFrames;
		}
	}

#if USEWARP
	// WARP
	string msgNumNotMatch = "Number of warps doesn't match the number of channels.";
	if (params.useWarpFile) {
		if (!warpController.loadSettings(params.warpFileName)) {
			cout << "Failed to load warp from \"S" + params.warpFileName + "\"'. Generating clean warps for each channel." << endl;
		}
		if (frames.size() != warpController.getNumWarps()) {
			cout << msgNumNotMatch << " Generating clean warps for each channel." << endl;
			params.useWarpFile = false;
		}
	}
	if (!params.useWarpFile && !generateWarp(params.NCHANNEL)) {
		cout << "Failed to generate warp(s). Exciting." << endl;
		ofExit(false);
		return;
	}
	if (frames.size() != warpController.getNumWarps()) {
		cout << msgNumNotMatch << " Exiting." << endl;
		ofExit(false);
		return;
	}
	warp.clear();
	for (int i = 0; i < params.NCHANNEL; i++) {
		warp.push_back(warpController.getWarp(i));
		warpSrc[i] = ofRectangle(0, 0, textures[i].getWidth(), textures[i].getHeight());
		warpDest[i] = frames[i];
		warpDest[i].position = ofPoint(0, 0);
		offsetWarp[i].set(0, 0);
	}
#endif

	bp.setDefaults();
	initGui();
}

//--------------------------------------------------------------
void ofApp::update() {
	if (ofGetFrameNum() < 2) {
		changedPositions = true; // well...
	}
	for (int i = 0; i < params.NCHANNEL; i++) {
		if (!isPaused) {
			vidGrabber[i].update();
		}
		if (vidGrabber[i].isFrameNew()) {
#if USEWARP
			textures[i].loadData(vidGrabber[i].getPixels()); // this resizes the texture to the video's w-h, if it was not big enough
#else
			imgs[i].setFromPixels(vidGrabber[i].getPixels());
#endif // USEWARP
		}

#if USEWARP
		
		if (resetWarp && i == currentFrame) {
			warp[i]->reset();
			resetWarp = false;
		}

		warpFbo[i].begin();
		if (warpOn || changedPositions) {
			ofClear(0, 0, 0, 0);
		}

		if (changedPositions) {
			imgs[i].allocate(frames[i].width, frames[i].height, OF_IMAGE_COLOR_ALPHA);
			warp[i]->setOffset(frames[i].position);
			warp[i]->setSize(frames[i].width, frames[i].height);
			warp[i]->handleWindowResize(frames[i].width, frames[i].height);
		}

		warp[i]->draw(textures[i], warpSrc[i] + offsetWarp[i], warpDest[i]);
		warpFbo[i].end();

		ofPixels pxls;
		warpFbo[i].readToPixels(pxls);
		imgs[i].setFromPixels(pxls);
#endif // USEWARP
		imgs[i].setImageType(OF_IMAGE_COLOR_ALPHA);
	}

	generateMerged();

	// blob tracking
	if (showBlobs) {
		if (changedPositions) {
			bt->saveParams(bp);
			delete bt;
			bt = new BLOBTRACKER(&imgMerged, bp, params.oscXml);
		}
		bt->update();
	}

	if (changedPositions) {
		relocateGui();
		guiUpdateLabels();
		changedPositions = false;
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	ofClear(255);
	//ofClear(255, 0, 0, 255);
	//ofEnableArbTex();
	ofSetHexColor(0xFFFFFF);

	ofPushStyle();
	ofSetHexColor(0x000000);
	string text = "FPS: " + ofToString(ofGetFrameRate());
	font.drawString(text, 20, 20);

	ofSetHexColor(warpOn ? 0x000000 : 0x888888);			text = "(W)arp";																font.drawString(text, 120, 20);
	ofSetHexColor(showFrames ? 0x000000 : 0x888888);		text = "(F)rames";																font.drawString(text, 250, 20);
	ofSetHexColor(highlightCurrent ? 0x000000 : 0x888888);	text = "(H)ighlight current";													font.drawString(text, 350, 20);
	ofSetHexColor(0x000000);								text = "(R)eset warp on current frame";	  										font.drawString(text, 550, 20);
	ofSetHexColor(grayResult ? 0x000000 : 0x888888);		text = "(G)ray result";															font.drawString(text, 780, 20);
	if (grayResult) {
		ofSetHexColor(0x000000);							text = "(E)xport to jpeg";														font.drawString(text, 900, 20);
	}
	ofSetHexColor(showBlobs ? 0x000000 : 0x888888);			text = "Show (b)lobs";															font.drawString(text, 1050, 20);
	ofSetHexColor(isPaused ? 0x000000 : 0xffffff);			text = "Paused";						  										font.drawString(text, 20, 40);
	ofSetHexColor(doCrop ? 0x000000 : 0xffffff);			text = "CROPPING";						  										font.drawString(text, 20, 60);
	if (showBlobs) {
		ofSetHexColor(bt->oscOn ? 0x000000 : 0x888888);
		for (int i = 0; i < bt->oscData.size(); i++) {
			text = "Sending blobs to host " + bt->oscData[i].name;	font.drawString(text, 100, 40 + 20 * i);
		}
	}
	ofSetHexColor(0x000000);								text = "(S)ave parameters";														font.drawString(text, 400, 40);
	ofSetHexColor(0x000000);								text = "S(a)ve parameters to file";												font.drawString(text, 540, 40);
	ofPopStyle();

	imgMerged.draw(getBoundaryRct(frames).position);

	if (showBlobs) {
		ofRectangle bound = getBoundaryRct(frames);
		bt->draw(bound.position, ofPoint(bound.x + bound.width, bound.y), font);
	}

	for (int i = 0; i < params.NCHANNEL; i++) {
		if (showFrames) {
			ofPushStyle();
			ofSetHexColor(0xff00ff);
			ofNoFill();
			ofDrawRectangle(frames[i]);
			ofPopStyle();
		}

		ofSetHexColor(0x33ff22);
#if IPCAM
		font.drawString(vidGrabber[i].getCameraName(), frames[i].x, frames[i].y + 10);
#else
		font.drawString("Cam " + ofToString(i + 1), frames[i].x, frames[i].y + 10);
#endif // IPCAM
		ofSetHexColor(0xffffff);
	}

	if (drawDrag) {
		ofPushStyle();
		ofNoFill();
		ofSetHexColor(0xFF8800);

		if (dragAll) {
			ofRectangle boundRct = getBoundaryRct(frames);
			ofDrawRectangle(dragPnt, boundRct.width, boundRct.height);
		}
		else if (!warpOn)
			ofDrawRectangle(dragPnt, frames[whichToDrag].width, frames[whichToDrag].height);

		ofPopStyle();
	}

	if (highlightCurrent) {
		if (isValidIdx(currentFrame)) {
			ofPushStyle();
			ofNoFill();
			ofSetHexColor(0x33cc33);
			ofDrawRectangle(frames[currentFrame]);
			ofPopStyle();
		}
	}

	// GUI
	for (int i = 0; i < params.NCHANNEL; i++) {
		xlbls[i].draw();
	}
	guiLbl.draw();
	guiDec.draw();
	guiInc.draw();

	guiBtnAction();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
	switch (key) {
	case ' ':
		isPaused = !isPaused;
#if VID_INSTEAD_CAM
		for (int i = 0; i < params.NCHANNEL; i++)
			vidGrabber[i].setPaused(isPaused);
#endif // VID_INSTEAD_CAM
		break;
	case 'e':
		if (grayResult)
			imgMerged.save("Img" + ofToString(nsaved++) + ".jpg", OF_IMAGE_QUALITY_BEST);
		break;
#if USEWARP
	case 'w':
		warpOn = !warpOn;
		if (warpOn && showBlobs) {
			keyPressed('b');
		}
		if (!warpOn) {
			changedPositions = true;
			drawDrag = false;
		}
		break;
#endif // USEWARP
	case 'f':
		showFrames = !showFrames;
		break;
	case 'r': // warp has it by default, but i won't remvove from here before the mouse tracing is ready
		if (doCrop && isValidIdx(currentFrame)) {
			offsetWarp[currentFrame].set(0, 0);
			changedPositions = true;
		}
		else {
			resetWarp = true;
		}
		break;
	case 's':
#if USEWARP
		this->warpController.saveSettings(params.warpFileName);
#endif // USEWARP
		params.saveToXml(frames);
		break;
	case 'h':
		highlightCurrent = !highlightCurrent;
		break;
	case 'g':
		grayResult = !grayResult;
		if (showBlobs)
			keyPressed('b');
		break;
	case 'b':
		showBlobs = !showBlobs;
		if (showBlobs) {
#if USEWARP
			if (warpOn) {
				warpOn = false;
				ofKeyEventArgs wkey;
				wkey.key = 'w';
				warpController.onKeyPressed(wkey);
			}
#endif // USEWARP
			bt = new BLOBTRACKER(&imgMerged, bp, params.oscXml);
		}
		else {
			bt->saveParams(bp);
			delete bt;
		}
		break;
	case 'o':
		bt->oscOn = !bt->oscOn;
		break;
	case 'a':
	{
		// probably windows bug that these parameters have no effect https://forum.openframeworks.cc/t/ofsystemsavedialog-defaultname-messagename-have-no-influence/15264

		ofFileDialogResult saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + "." + ofToLower("xml"), "Save your parameters");
		if (saveFileResult.bSuccess) {
			params.saveToXml(frames, saveFileResult.filePath);
		}
#if USEWARP
		saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + "." + ofToLower("json"), "Save your warps");
		if (saveFileResult.bSuccess) {
			this->warpController.saveSettings(saveFileResult.filePath);
		}
#endif // USEWARP
	}
		break;
#if USEWARP
	case 'c':
		doCrop = true;
		break;
#endif;
	
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	switch (key) {
#if USEWARP
	case 'c':
		doCrop = false;
		break;
#endif;
	case OF_KEY_LEFT:
		if (isValidIdx(currentFrame)) {
			if (doCrop) {
				offsetWarp[currentFrame].x++;
			}
			else {
				frames[currentFrame].x--;
			}
			changedPositions = true;
		}
		break;
	case OF_KEY_RIGHT:
		if (isValidIdx(currentFrame)) {
			if (doCrop) {
				offsetWarp[currentFrame].x--;
			}
			else {
				frames[currentFrame].x++;
			}
			changedPositions = true;
		}
		break;
	case OF_KEY_UP:
		if (isValidIdx(currentFrame)) {
			if (doCrop) {
				offsetWarp[currentFrame].y++;
			}
			else {
				frames[currentFrame].y--;
			}
			changedPositions = true;
		}
		break;
	case OF_KEY_DOWN:
		if (isValidIdx(currentFrame)) {
			if (doCrop) {
				offsetWarp[currentFrame].y--;
			}
			else {
				frames[currentFrame].y++;
			}
			changedPositions = true;
		}
		break;
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {
	currentFrame = -1;
	int n = 0;
	for (int i = 0; i<params.NCHANNEL; i++) {
		if (frames[i].inside(ofPoint(x, y))) {
			if (n) { // more than 1 frames under cursor, do nothing
				currentFrame = -1;
				return;
			}
			n++;
			currentFrame = i;
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
	if (warpOn) {
#if USEWARP
		int idxActiveWarp = -1;
		int nw = warpController.getNumWarps();
		for (int i = 0; i < nw; i++) {
			auto wrp = warpController.getWarp(i);
			int idx = wrp->getSelectedControlPoint();
			if (idx > 0) {
				idxActiveWarp = i;
			}
		}
		if (isValidIdx(idxActiveWarp)) {
			if (!frames[idxActiveWarp].inside(ofPoint(x, y))) {
				auto wrp = warpController.getWarp(idxActiveWarp);
				wrp->deselectControlPoint();
			}
		}
#endif
	}
	else if(!doCrop) {
		if (button == 0) {
			int n = 0;
			for (int i = 0; i < params.NCHANNEL; i++) {
				if (frames[i].inside(dragStart)) {
					n++;
					if (n > 1) { // more than 1 frames under cursor, do nothing
						drawDrag = false;
						return;
					}
					dragPnt = ofPoint(x, y) - dragStart + frames[i].getPosition();
				}
			}
			if (n) {
				drawDrag = true;
			}
		}
		if (button == 2) {
			ofRectangle rctBound = getBoundaryRct(frames);
			if (rctBound.inside(dragStart)) {
				drawDrag = true;
				dragAll = true;
				dragPnt = ofPoint(x, y) - dragStart + rctBound.getPosition();
			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
	if (!warpOn) {
		dragStart = ofPoint(x, y);
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {
	if (!warpOn && button == 0) {
		int n = 0;
		for (int i = 0; i < params.NCHANNEL; i++) {
			if (frames[i].inside(dragStart)) {
				if (n) return;
				n++;
				whichToDrag = i;
			}
		}

		if (n) { // case of dragging
			dragRel = ofPoint(x, y) - dragStart;
			if (doCrop) {
				offsetWarp[whichToDrag] -= dragRel;
			}
			else {
				frames[whichToDrag].position += dragRel;
				drawDrag = false;
			}
			changedPositions = true;
		}
	}
	if (!warpOn && button == 2) {
		ofRectangle rctBound = getBoundaryRct(frames);
		if (rctBound.inside(dragStart)) {
			dragRel = ofPoint(x, y) - dragStart;
			for (int i = 0; i < params.NCHANNEL; i++) {
				frames[i].position += dragRel;
			}
			drawDrag = false;
			dragAll = false;
			changedPositions = true;
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}

//void ofApp::exit() {
//
//}


//--------------------------------------------------------------
void ofApp::initMapLayout() {
	int maxh = 0, maxw = 0;

	for (int i = 0; i < params.NCHANNEL; i++) {
		if (frames[i].width > maxw)
			maxw = frames[i].width;
		if (frames[i].height > maxh)
			maxh = frames[i].height;
	}

	for (int i = 0; i < params.camMap.size(); i++) {
		for (int j = 0; j < params.camMap[i].size(); j++) {
			if (params.camMap[i][j] > 0) {
				frames[params.camMap[i][j] - 1].x = j*maxw;
				frames[params.camMap[i][j] - 1].y = i*maxh + 40;
			}
		}
	}
	changedPositions = true;
}

//--------------------------------------------------------------
ofApp::~ofApp() {
	delete[] vidGrabber;
	delete[] imgs;
#if USEWARP
	delete[] textures;
	delete[] warpFbo;
	delete[] warpSrc;
	delete[] warpDest;
#endif // USEWARP
	delete[] xlbls;
	delete[] ylbls;
	delete[] btnDec;
	delete[] btnInc;

	//if(bt != NULL)
	//	delete bt;
}

//--------------------------------------------------------------
void ofApp::generateMerged() {
	ofRectangle area;
	area = getBoundaryRct(frames);
	if (grayResult) {
		imgMerged.allocate(area.width, area.height, OF_IMAGE_GRAYSCALE);
	}
	else {
		imgMerged.allocate(area.width, area.height, OF_IMAGE_COLOR_ALPHA);
	}
	ofPoint act, vidPt;
	for (int x = 0; x<area.width; x++) {
		for (int y = 0; y<area.height; y++) {
			act = ofPoint(area.x + x, area.y + y);
			int n = 0, r = 0, g = 0, b = 0, alpha = 0;
			bool isAlpha = false;

			for (int i = 0; i<frames.size(); i++) {
				if (frames[i].inside(act)) {
					ofPixels &pxls = imgs[i].getPixels();
					vidPt = act - frames[i].position;

					int px = 4 * (frames[i].width * vidPt.y + vidPt.x);
					alpha = pxls[px + 3];
					if (alpha > 0) {
						isAlpha = true;
						r += pxls[px];
						g += pxls[px + 1];
						b += pxls[px + 2];
						n++;
					}
				}
			}
			if (n>1) {
				r /= n;
				g /= n;
				b /= n;
			}
			alpha = isAlpha ? 255 : 0;

			if (grayResult) {
				int val = (r + g + b) / 3;
				if (!alpha) val = 0;
				int pxl = area.width*y + x;
				imgMerged.getPixels()[pxl] = val;
			}
			else {
				int pxl = 4 * (area.width*y + x);
				imgMerged.getPixels()[pxl] = r;
				imgMerged.getPixels()[pxl + 1] = g;
				imgMerged.getPixels()[pxl + 2] = b;
				imgMerged.getPixels()[pxl + 3] = alpha;
			}
		}
	}
	if (grayResult) {
		imgMerged.setImageType(OF_IMAGE_GRAYSCALE);
	}
	else {
		imgMerged.setImageType(OF_IMAGE_COLOR_ALPHA);
	}
}

//--------------------------------------------------------------
void ofApp::guiBtnAction() {
	int i = 0;
	int change = 0;
	for (; i < 2 * params.NCHANNEL; i++) {
		ofMouseEventArgs ea;
		ea.button = 0;
		if (btnDec[i].mouseReleased(ea)) {
			change = -1;
			break;
		}
		else if (btnInc[i].mouseReleased(ea)) {
			change = 1;
			break;
		}
	}

	if (change == 0)
		return;

	if (i < params.NCHANNEL) {
		if (doCrop) {
			offsetWarp[i].x -= change;
		}
		else {
			frames[i].x += change;
		}
		whichToDrag = i;
	}
	else {
		if (doCrop) {
			offsetWarp[i - params.NCHANNEL].y -= change;
		}
		else {
			frames[i - params.NCHANNEL].y += change;
		}
		whichToDrag = i - params.NCHANNEL;
	}
	changedPositions = true;
}

//--------------------------------------------------------------
void ofApp::guiUpdateLabels() {
	for (int i = 0; i < params.NCHANNEL; i++) {
#if IPCAM
		xlbls[i].setup(vidGrabber[i].getCameraName() + " X: ", ofToString(frames[i].x));
		ylbls[i].setup(vidGrabber[i].getCameraName() + " Y: ", ofToString(frames[i].y));
#else
		xlbls[i].setup("Cam " + ofToString(i + 1) + " X: ", ofToString(frames[i].x));
		ylbls[i].setup("Cam " + ofToString(i + 1) + " Y: ", ofToString(frames[i].y));
#endif
	}
}
#if USEWARP
// generates a JSON description for n clean warps
//--------------------------------------------------------------
string ofApp::createJson(int n, bool framePos) {
	string retVal = "";
	retVal += R"verbatim(
	{
		"warps": [
)verbatim";

	for (int i = 0; i < n; i++) {
		retVal += R"verbatim(
		{
      "adaptive": true,
      "blend": {
        "edges": "0, 0, 0, 0",
        "exponent": 2,
        "gamma": "1, 1, 1",
        "luminance": "0.5, 0.5, 0.5"
      },
      "brightness": 1,
      "corners": [
)verbatim";

		string edges = "";
		ofRectangle vidSpace = getBoundaryRct(frames);
		float w = vidSpace.width;
		float h = vidSpace.height;
		if (framePos) {
			edges = "\"" + ofToString(frames[i].x / w) + ", " + ofToString(frames[i].y / h) + "\",\n"
				"\"" + ofToString((frames[i].x + frames[i].width) / w) + ", " + ofToString(frames[i].y / h) + "\",\n" +
				"\"" + ofToString((frames[i].x + frames[i].width) / w) + ", " + ofToString((frames[i].y + frames[i].height) / h) + "\",\n" +
				"\"" + ofToString(frames[i].x / w) + ", " + ofToString((frames[i].y + frames[i].height) / h) + "\"\n";
		}
		else {
			edges = "\"" + ofToString(params.warpEdge) + ", " + ofToString(params.warpEdge) + "\",\n"
				"\"" + ofToString(1 - params.warpEdge) + ", " + ofToString(params.warpEdge) + "\",\n" +
				"\"" + ofToString(1 - params.warpEdge) + ", " + ofToString(1 - params.warpEdge) + "\",\n" +
				"\"" + ofToString(params.warpEdge) + ", " + ofToString(1 - params.warpEdge) + "\"\n";
		}

		retVal += edges;

		retVal += R"verbatim(
      ],
      "linear": false,
      "resolution": 1,
      "type": 3,
      "warp": {
        "columns": 3,
        "control points": [
          "0, 0",
          "0, 0.5",
          "0, 1",
          "0.5, 0",
          "0.5, 0.5",
          "0.5, 1",
          "1, 0",
          "1, 0.5",
          "1, 1"
        ],
        "rows": 3
      }
    }
				)verbatim";

		if (i < n - 1) {
			retVal += ",\n";
		}
	}

	retVal += R"verbatim(
		]
	}
	)verbatim";

	return retVal;
}
#endif


// AUX
#if USEWARP
//--------------------------------------------------------------
bool ofApp::generateWarp(const int& nWarp) {
	if (nWarp == 0)
		return false;
	string json = createJson(nWarp, false);
	string tmpJson = "tmp.json";
	ofFile jsonFile = ofFile(tmpJson, ofFile::WriteOnly);
	jsonFile << json;
	jsonFile.close();
	bool loaded = warpController.loadSettings(tmpJson);
	jsonFile.removeFile(tmpJson);
	return loaded;
}
#endif

//--------------------------------------------------------------
ofRectangle getBoundaryRct(const vector<ofRectangle> &rcts) {
	if (rcts.size() == 0) return ofRectangle(0, 0, 0, 0);
	ofPoint min(ofGetWidth(), ofGetHeight()), max(0, 0);
	for (int i = 0; i<rcts.size(); i++) {
		if (rcts[i].x<min.x) min.x = rcts[i].x;
		if (rcts[i].y<min.y) min.y = rcts[i].y;
		if (rcts[i].x + rcts[i].width>max.x) max.x = rcts[i].x + rcts[i].width;
		if (rcts[i].y + rcts[i].height>max.y) max.y = rcts[i].y + rcts[i].height;
	}
	return ofRectangle(min, max);
}

//--------------------------------------------------------------
ofRectangle getBoundaryRct(const ofRectangle & rct, const ofPoint& pnt) {
	if (rct.inside(pnt))
		return rct;
	float	minx = pnt.x<rct.x ? pnt.x : rct.x,
		miny = pnt.y<rct.y ? pnt.y : rct.y,
		maxx = pnt.x>(rct.x + rct.width) ? pnt.x : (rct.x + rct.width),
		maxy = pnt.y>(rct.y + rct.height) ? pnt.y : (rct.y + rct.height);
	return ofRectangle(ofPoint(minx, miny), ofPoint(maxx, maxy));
}

#if USEWARP
//--------------------------------------------------------------
ofRectangle getWarpBoundary(shared_ptr<ofxWarpBase> warp, bool norm) {
	float minx = 1, miny = 1;
	float maxx = 0, maxy = 0;
	int ncp = warp->getNumControlPoints();
	for (int i = 0; i < ncp; i++) {
		ofPoint pnt = warp->getControlPoint(i);
		if (pnt.x < minx)minx = pnt.x;
		if (pnt.y < miny)miny = pnt.y;
		if (pnt.x > maxx)maxx = pnt.x;
		if (pnt.y > maxy)maxy = pnt.y;
	}
	if (!norm) {
		minx *= ofGetWidth();
		maxx *= ofGetWidth();
		miny *= ofGetHeight();
		maxy *= ofGetHeight();
	}
	return ofRectangle(minx, miny, maxx - minx, maxy - miny);
}
#endif // USEWARP

inline bool ofApp::isValidIdx(const int& idx) {
	return idx > -1 && idx < params.NCHANNEL;
}

#if IPCAM
bool ofApp::LoadIpCams(const string &xmlName, vector<float> &ws, vector<float> &hs) {
	ofxXmlSettings xmlSettings;
	if (!xmlSettings.load(xmlName)) {
		cout << "Error loading XML." << endl;
		return false;
	}
	//vidGrabber.clear();
	vector<string> uris;
	vector<string> names;
	string str = "cameras";
	if (xmlSettings.getNumTags(str) > 0) {
		xmlSettings.pushTag(str);
		str = "camera";
		int nhost = xmlSettings.getNumTags(str);
		ws.clear();
		hs.clear();
		for (int i = 0; i < nhost; i++) {
			cout << i << endl;
			string uri = xmlSettings.getValue(str + ":uri", "", i);
			string name = xmlSettings.getValue(str + ":name", "", i);
			ws.push_back(xmlSettings.getValue(str + ":width", 0, i));
			hs.push_back(xmlSettings.getValue(str + ":height", 0, i));
			uris.push_back(uri);
			names.push_back(name);
		}
	}

	if (uris.size() != names.size()) {
		cout << "Invalid XML file: " + xmlName << endl;
		return false;
	}

	if (uris.size() != params.NCHANNEL) {
		cout << "The number of cameras specified in " + xmlName + " doesn't match the configuration." << endl;
		return false;
	}

	vidGrabber = new ofx::Video::IPVideoGrabber[uris.size()];
	for (int i = 0; i < uris.size(); i++) {
		vidGrabber[i].setCameraName(names[i]);
		vidGrabber[i].setURI(uris[i]);
		vidGrabber[i].connect();
	}

	return true;
}
#endif // IPCAM