#include "parameters.h"

PARAMETERS::PARAMETERS(){
	setDefaultConfig();
}

PARAMETERS::~PARAMETERS(){
}

void PARAMETERS::setDefaultConfig() {
	xmlFileName = "";
	useXml = false;
	useMap = false;
	mapFile = "";
	NCHANNEL = 0; // to be synchronized with camera inputs
	useWarpFile = false;
	warpFileName = "";
	warpEdge = 0;
}

bool PARAMETERS::readConfig(const string &fileName) {
	ifstream file;
	file.open(fileName, ios::in);
	if (!file.is_open())
		return false;
	string item = "";
	
	file >> item;
	if (item == "usexml")
		file >> useXml;
	else return false;

	file >> item;
	if (item == "xmlfilename")
		file >> xmlFileName;
	else return false;

	file >> item;
	if (item == "usemap")
		file >> useMap;
	else return false;

	file >> item;
	if (item == "mapfile")
		file >> mapFile;
	else return false;

	file >> item;
	if (item == "usewarpfile")
		file >> useWarpFile;
	else return false;

	file >> item;
	if (item == "warpfilename")
		file >> warpFileName;
	else return false;

	file >> item;
	if (item == "warpedge")
		file >> warpEdge;
	else return false;

	file >> item;
	if (item == "oscxml")
		file >> oscXml;
	else return false;

	file >> item;
	if (item == "ipcamxml")
		file >> ipCamXml;
	else return false;

	return true;
}

bool PARAMETERS::readCamMap() {
	// >0 numbers mean the number of camera. if 0, then empty slot.
	// we don't check, if the sequence, or the h/w numbers are correct.
	ifstream file;
	file.open(mapFile, ios::in);
	if (!file.is_open())
		return false;
	NCHANNEL = 0;
	int nx, ny;
	file >> ny >> nx;
	camMap.resize(ny);
	for (int i = 0; i < ny; i++) {
		camMap[i].resize(nx);
		for (int j = 0; j < nx; j++) {
			file >> camMap[i][j];
			if (camMap[i][j]) {
				NCHANNEL++;
			}
		}
	}
	file.close();
	return true;
}

void PARAMETERS::saveToXml(const std::vector<ofRectangle> &frames, const string &name) {
	xml.clear();
	xml.addChild("parameters");

	ofXml framesXml;
	framesXml.clear();
	framesXml.addChild("frames");

	for (int i = 0; i < frames.size(); i++) {
		ofXml frameXml;
		frameXml.clear();
		frameXml.addChild("frame");
		//frameXml.setTo("frame");

		frameXml.addValue("x", frames[i].x);
		frameXml.addValue("y", frames[i].y);
		frameXml.addValue("width", frames[i].width);
		frameXml.addValue("height", frames[i].height);
		framesXml.addXml(frameXml);
	}

	xml.addXml(framesXml);

	// blob could come here

	//xml.addChild("warp");
	//xml.setTo("warp");
	//xml.addValue("filename", saveWarpFile);
	if(name=="")
		xml.save(xmlFileName == "" ? "parameters.xml" : xmlFileName);
	else
		xml.save(name);
}

bool PARAMETERS::loadFromXml(vector<ofRectangle> &frames) {
	if (!xmlLoad.load(xmlFileName)) {
		cout << "Error loading XML." << endl;
		return false;
	}
	string str = "parameters";
	if (xmlLoad.getNumTags(str) > 0) {
		xmlLoad.pushTag(str);
		str = "frames";
		if(xmlLoad.getNumTags(str) > 0){
			xmlLoad.pushTag(str);
			str = "frame";
			NCHANNEL = xmlLoad.getNumTags(str);
			for (int i = 0; i < NCHANNEL; i++) {
				frames.push_back(ofRectangle(
					xmlLoad.getValue(str + ":x", 0, i),
					xmlLoad.getValue(str + ":y", 0, i),
					xmlLoad.getValue(str + ":width", 0, i),
					xmlLoad.getValue(str + ":height", 0, i)
				));
			}
		}

		// blob could come here

		//xmlLoad.popTag();
		//str = "warp";
		//if (xmlLoad.getNumTags(str) > 0) {
		//	readWarpFile = xmlLoad.getValue(str + ":filename", "generated.json", 0);
		//}
		//else {
		//	useWarpFile = false;
		//}
	}
	useMap = false;
	return true;
}