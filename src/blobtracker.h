#pragma once

#include "ofxOpenCv.h"
#include "ofxBlobsManager.h"
#include "ofxCv.h"
#include "ofxGui.h"
#include "ofxOsc.h"
#include "ofXml.h"
#include "ofxXmlSettings.h"

struct BLOBPARAMS {
	int thres;
	int blur;
	float normperc;
	int maxUndetectedTime;
	int minDetectedTime;

	void set(const BLOBPARAMS &bp);
	void set(int th, int bl, float np, int mut, int mdt);
	void setDefaults();
};

struct OSCDATA {
	string ip;
	string name;
	int port;
	OSCDATA(const string &i, const string &n, const int &p) { ip = i; name = n; port = p; }
	bool operator == (const OSCDATA &oscd) { return oscd.name == name; }
};

class BLOBTRACKER
{
public:
	BLOBTRACKER(ofImage *img);
	BLOBTRACKER(ofImage *img, const BLOBPARAMS &bp, const string &oscXml);
	~BLOBTRACKER();

	void reset();
	void reset(ofImage *img);
	void update();
	template<typename vec2>
		void draw(vec2 v1, vec2 v2, vec2 v3);
	template<typename vec2>
		void draw(vec2 v);
	template<typename vec2>
		void draw(vec2 v, vec2 guiLoc, const ofTrueTypeFont &fnt);
	ofImage *srcImg;
	void saveParams(BLOBPARAMS &bp);

	ofxCv::RunningBackground background;
	ofFbo fboBlob;
	ofFbo fboDiff;

	ofPixels pixels;

	int width;
	int height;

	bool init = false;
	bool bLearnBakground;

	bool drawIntermediates = false;

	ofxCvColorImage colorImg;

	ofImage backgroundEl;

	ofxCvGrayscaleImage grayImage;
	ofxCvGrayscaleImage grayBg;
	ofxCvGrayscaleImage grayDiff;

	ofxCvContourFinder contourFinder;

	ofxBlobsManager blobsManager;

	int threshold;
	int blur;

	// GUI
	vector<string> guiNames;
	ofxPanel guiLbl;
	ofxPanel guiDec;
	ofxPanel guiInc;
	vector<ofxLabel> labels;
	vector<ofxButton> btnDec;
	vector<ofxButton> btnInc;
	void initGui();
	void drawGui(const ofVec2f &place);
	void guiBtnAction();
	void setGuiValues();


	// OSC
	string oscXmlName;
	bool loadOscData();
	bool oscOn;
	vector<ofxOscSender> blobSender;
	void sendBlobs();
	vector<OSCDATA> oscData;
};

template<typename vec2>
void BLOBTRACKER::draw(vec2 v) {
	for (int i = 0; i < contourFinder.blobs.size(); i++) {
		contourFinder.blobs[i].draw(v.x, v.y);
	}
}

template<typename vec2>
void BLOBTRACKER::draw(vec2 v, vec2 guiLoc, const ofTrueTypeFont &fnt) {
	for (int i = 0; i < contourFinder.blobs.size(); i++) {
		contourFinder.blobs[i].draw(v.x, v.y);
	}
	drawGui(guiLoc);
	guiBtnAction();

	vec2 blobDataLoc = guiLoc + vec2(20, 160);
	ofPushStyle();
	ofSetHexColor(0x000000);
	fnt.drawString("Nblobs: " + ofToString(contourFinder.blobs.size()), blobDataLoc.x, blobDataLoc.y);

	for (int i = 0; i < contourFinder.blobs.size(); i++) {
		fnt.drawString(ofToString(i + 1) + ". x: " + ofToString(contourFinder.blobs[i].centroid.x/width) + "   y: " + ofToString(contourFinder.blobs[i].centroid.y/height), blobDataLoc.x, blobDataLoc.y + (i + 1) * 20);
	}

	ofPopStyle();
}

template<typename vec2>
void BLOBTRACKER::draw(vec2 v1, vec2 v2, vec2 v3) {
	ofPushStyle();
	ofSetHexColor(0xFFFFFF);

	if (drawIntermediates) {
		grayImage.draw(v2);
		grayDiff.draw(v2);
	}
	//vidGrabber.draw(320, 0);
	draw(v3);

	for (int i = 0; i < blobsManager.blobs.size(); i++)	{
		ofxStoredBlobVO blob = blobsManager.blobs.at(i);
	}
	ofPopStyle();
}
 