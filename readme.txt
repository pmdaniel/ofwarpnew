------------------------------------------------------------
For compiling you MUST have a file called pmswitches.h in the src folder, containing the following options:

#pragma once

#define VID_INSTEAD_CAM 0 // default 0
#define IPCAM 0 && !VID_INSTEAD_CAM // default 1
#define USEWARP 1 // default 1

------------------------------------------------------------
Config file (pmconfig):
Without a config file named 'pmconfig' placed in data/, the program won't run. 'pmconfig' must have the following structure (without comments):
- usexml	1	// False: whatever ->usemap brings
- xmlfilename	parameters.xml	//if(usexml): frame locations will be loaded from the specified file. If loading fails, then the program will look for ->mapfile.
- usemap	1
- mapfile	data/cam.map	// if(usemap), then the image positions will be loaded from mapfile. If it fails, then the images will appear in a stright line.
- usewarpfile	1	// True: warps will be loaded from and saved to ->warpfilename. False: clean warps will be generated.
- warpfilename	generated.json	// if(usewarpfile), then warps will be loaded from readwarpfile. If loading fails or the number of loaded warps doesn't match the number of images, clean warps will be generated for each image.
- warpedge	0.1 // if generating warps, then warpEdge will be the offset ratio of the warps from the frame edges. Keep it in the 0..0.5 interval in order to have a straight image that doesn't exceed its frame.
- oscxml	oscparams.xml // this file contains the parameters for sending blobs with OSC.
- ipcamxml	camips.xml // this file contains the names, addresses and resolution info for the IP cameras.

------------------------------------------------------------
Map file (cam.map):
The program expects a rectangular grid of >=0 numbers. In the first line specify the number of rows and columns, respecively. Below the grid:
3	4
0	2	0	0
1	0	0	4
0	5	3	0
If the number is 0, it means a gap, while the >0 numbers must form a continuous sequence from 1 to the number of images. If there is a valid map file, and no xml is used, the images will appear according to this map.

------------------------------------------------------------
Control keys:
W - switches between warping and dragging mode. In dragging mode, if 'C' is pressed, then the image will move inside the frame, otherwise the whole frame will move.
F - highlight the boundaries of the participating frames.
B - changes method of blending black color. BUG: not perfect yet
H - highliths the frame under mouse cursor, if it's unique.
R - resets the warp on current frame (see 'H'). BUG: tends to fail.
B - show blobs. Toggles blob tracking on and off. For blob tracking, warping is turned off automatically, as the blinking control points would disturb the blob tracking.
O - turns on/off sending blob parameters trhough OSC
S - save current positions, frame sizes (to xml) and warp configuration (to json) to the xml and warp files specified in ->pmconfig.
A - save current positions, frame sizes (to xml) and warp configuration (to json) to files specified in the dialogs.
(space) - pause.

------------------------------------------------------------
GUI:
displays the coordinates of the upper left corner of each frame. Also allows to explicitly modify them, making the frames move.

------------------------------------------------------------
Bugs:
- resetting warp only straightens the warp, doesn't eliminate the perspective.
- gui control placement is random sometimes
- cropping doesn't leave the background transparent

------------------------------------------------------------
Todo:
- increase fps
- why are there no more than 10 blobs allowed at once?
- background substraction